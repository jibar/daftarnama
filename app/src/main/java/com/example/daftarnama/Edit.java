package com.example.daftarnama;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class Edit extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle("Edit Data");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        Intent intent  = getIntent();
        String nim = intent.getStringExtra("nim");
        TextView a =findViewById(R.id.vnim);
        a.setText("NIM         :"+nim);
    }
    public void save(View view) {
        EditText nama = findViewById(R.id.edNama);
        EditText alamat = findViewById(R.id.edAlamat);
        EditText hp = findViewById(R.id.edHp);
        Intent get = getIntent();
        int posisi = get.getIntExtra("posisi",-1);
        Intent save = new Intent(getApplicationContext(),MainActivity.class);
        save.putExtra("NM",nama.getText().toString());
        save.putExtra("ALM",alamat.getText().toString());
        save.putExtra("HP",hp.getText().toString());
        save.putExtra("posisi",posisi);
        startActivity(save);
    }
}
