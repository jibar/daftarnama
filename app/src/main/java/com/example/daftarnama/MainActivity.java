package com.example.daftarnama;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {
    private GridView gv;
    private String[] nim ={
            "F1D016046",
            "F1D016021",
            "F1D016001",
            "F1D016003",
            "F1D016005",
            "F1D016007"
    };
    private String[] nama ={
            "Jibar",
            "Dimas",
            "Ahmad",
            "Wawan",
            "Damid",
            "Ismail"
    };
    private String[] alamat ={
            "Bima",
            "Mataram",
            "Sekarbela",
            "Gomong sakura",
            "Mataram",
            "Mars"
    };
    private String[] hp ={
            "088888888",
            "099999999",
            "077777777",
            "066666666",
            "055555555",
            "044444444"
    };
    int post = -1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gv=findViewById(R.id.GV);
        Intent get = getIntent();
        post = get.getIntExtra("posisi", -1);
        if(post>-1) {
            nama[post] = get.getStringExtra("NM");
            alamat[post] = get.getStringExtra("ALM");
            hp[post] = get.getStringExtra("HP");
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                MainActivity.this,
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                nim
        );

        gv.setAdapter(adapter);
        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent=new Intent(getApplicationContext(), Hasil.class);
                intent.putExtra("nim",nim[position]);
                intent.putExtra("nama",nama[position]);
                intent.putExtra("alamat",alamat[position]);
                intent.putExtra("hp",hp[position]);
                intent.putExtra("posisi",position);
                post = -1;
                //Intent edit=new Intent(getApplicationContext(), Edit.class);
                //edit.putExtra("nim",nim[position]);
                startActivity(intent);
            }
        });

    }
}
