package com.example.daftarnama;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Hasil extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasil);
        Intent intent=getIntent();
        String nim=intent.getStringExtra("nim");
        String nama=intent.getStringExtra("nama");
        String alamat=intent.getStringExtra("alamat");
        String hp=intent.getStringExtra("hp");
        TextView a=findViewById(R.id.xnim);
        TextView b=findViewById(R.id.xnama);
        TextView c=findViewById(R.id.xalamat);
        TextView d=findViewById(R.id.xhp);

        a.setText(nim);
        b.setText(nama);
        c.setText(alamat);
        d.setText(hp);
        setTitle("Data Lengkap");
    }

    public void pindah(View view) {
        Intent intent = getIntent();
        int posisi = intent.getIntExtra("posisi",-1);
        String nim = intent.getStringExtra("nim");
        Intent pindah = new Intent(getApplicationContext(),Edit.class);
        pindah.putExtra("nim",nim);
        pindah.putExtra("posisi",posisi);
        startActivity(pindah);
    }
}
